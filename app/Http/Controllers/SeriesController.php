<?php

namespace App\Http\Controllers;

use App\Series;
use Illuminate\Http\Request;
use DB;

class SeriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public static function getAllSeries()
    {
        $data = DB::select('select * from series order by created_at DESC');

        if(count($data) > 0)
        {
            return $data;
        }
        else
        {
            return null;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            "inputSeriesAuthor"  =>  'required',
            "inputSeriesTitle"  =>  'required',
            "inputSeriesSummary"  =>  'required'
        ]);

        $series = new Series([
            'title'    =>  $request->get("inputSeriesTitle"),
            'author'    =>  $request->get("inputSeriesAuthor"),
            'summary'    =>  $request->get("inputSeriesSummary"),
        ]);
        $series->save();
        return view('admin.series.add-series')->with('success', 'Series Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Series  $series
     * @return \Illuminate\Http\Response
     */
    public function show(Series $series)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Series  $series
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $series = Series::find($id);
        $id = $id;
        return view('admin.series.edit-series', compact('series', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Series  $series
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            "inputSeriesAuthor"  =>  'required',
            "inputSeriesTitle"  =>  'required',
            "inputSeriesSummary"  =>  'required'
        ]);

        $series = Series::find($id);
        $series->author = $request->get('inputSeriesAuthor');
        $series->title = $request->get('inputSeriesTitle');
        $series->summary = $request->get('inputSeriesSummary');
        $series->save();
        return view('admin.series.edit-series', compact('series', 'id'))->with('success', 'Series Edited');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Series  $series
     * @return \Illuminate\Http\Response
     */
    public function destroy(Series $series)
    {
        //
    }
}
