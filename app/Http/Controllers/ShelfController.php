<?php

namespace App\Http\Controllers;

use App\Shelf;
use Illuminate\Http\Request;
use DB;
use Auth;

class ShelfController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            "inputShelfName"  =>  'required'
        ]);

        $userID = $request->get("inputUserID");

        $shelf = new Shelf([
            'name'   =>   $request->get("inputShelfName"),
            'user_id'   =>   $userID,
        ]);
        $shelf->save();

        $shelfData = DB::select('select * from shelves where user_id=:id', ['id' => $userID]);
        if($shelfData!=null)
        {
            $allShelves = json_decode(json_encode($shelfData), true);
        }

        $data = [
            'userID'   =>   $request->get("inputUserID"),
            'allShelves'    => $allShelves,
            'success'  => 'Shelf Added'
        ];
        return view('user.shelves.all-shelves')->with($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Shelf  $shelf
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $shelfData = DB::select('select * from shelves where user_id=:id', ['id' => $id]);
        if($shelfData!=null)
        {
            $allShelves = json_decode(json_encode($shelfData), true);
        }
        $userID = $id;
        return view('user.shelves.all-shelves', compact('allShelves', 'userID'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Shelf  $shelf
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $shelf = Shelf::find($id);
        $id = $id;
        return view('user.shelves.edit-shelf', compact('shelf', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Shelf  $shelf
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            "inputShelfName"  =>  'required',
        ]);

        $shelf = Shelf::find($id);
        $shelf->name = $request->get('inputShelfName');
        $shelf->save();
        return view('user.shelves.edit-shelf', compact('shelf', 'id'))->with('success', 'Shelf Edited');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shelf  $shelf
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $shelf = Shelf::find($id);
        $shelf->delete();
        $userID = Auth::id();
        return view('user.shelves.all-shelves', compact('userID'))->with('success', 'Shelf Deleted');
    }
}
