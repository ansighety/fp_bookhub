<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function admin()
    {
        $allBooks = BooksController::getAllBooks();
        $allSeries = SeriesController::getAllSeries();
        if($allBooks!=null)
        {
            $allBooks = json_decode(json_encode($allBooks), true);
        }
        if($allSeries!=null)
        {
            $allSeries = json_decode(json_encode($allSeries), true);
        }
        return view('admin.admin-panel', compact('allBooks','allSeries'));

        /*
        $allSeries = SeriesController::getAllSeries();
        $allWorks = WorksController::getAllWorks();
        if($allSeries!=null)
        {
            $allSeries = json_decode(json_encode($allSeries), true);
        }
        if($allWorks!=null)
        {
            $allWorks = json_decode(json_encode($allWorks), true);
        }
        return view('admin/dashboard', compact('allSeries', 'allWorks'));
        */
    }
}
