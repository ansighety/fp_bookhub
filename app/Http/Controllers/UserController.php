<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = DB::select('select * from users where id=:id', ['id' => $id]);
        if($data!=null)
        {
            $data = json_decode(json_encode($data), true);
        }
        $userData = $data[0];

        return view('user.profile-settings', compact('userData'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $currentUser = Auth::user();

        /** IF CORRECT PASSWORD */
        if($currentUser->password == bcrypt($request->get('inputUserPassword')))
        {
            $currentUser->email = $request->get('inputUserEmail');
            $currentUser->name = $request->get('inputUserName');

            $currentUser->save();
            $userData = $currentUser;
            return view('profile-settings', compact('userData'))->with('success', 'Profile Updated');
        }
        else
        {
            $userData = $currentUser;
            return view('profile-settings', compact('userData'))->with('error', 'Password is incorrect. Please try again.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
