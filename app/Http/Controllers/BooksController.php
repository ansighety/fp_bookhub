<?php

namespace App\Http\Controllers;

use App\Books;
use Illuminate\Http\Request;
use DB;

class BooksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    public static function getAllBooks()
    {
        $data = DB::select('select * from books order by created_at DESC');

        if(count($data) > 0)
        {
            return $data;
        }
        else
        {
            return null;
        }
    }

    public function getAllWorks()
    {
        $allBooks = DB::select('select * from books order by title ASC');
        $allSeries = DB::select('select * from series order by title ASC');

        if($allBooks!=null)
        {
            $allBooks = json_decode(json_encode($allBooks), true);
        }
        if($allSeries!=null)
        {
            $allSeries = json_decode(json_encode($allSeries), true);
        }
        return view('all-works', compact('allBooks','allSeries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            "inputBookAuthor"  =>  'required',
            "inputBookTitle"  =>  'required',
            "inputBookSummary"  =>  'required'
        ]);

        $book = new Books([
            'author'    =>  $request->get("inputBookAuthor"),
            'title'    =>  $request->get("inputBookTitle"),
            'summary'    =>  $request->get("inputBookSummary"),
            'type'  =>  $request->get("inputBookType"),
            'expected_number_of_chapters'    =>  $request->get("inputBookExpectedChapters"),
            'shared'    =>  $request->get("inputBookShared"),
        ]);
        $book->save();
        return view('admin.books.add-book')->with('success', 'Book Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Books  $books
     * @return \Illuminate\Http\Response
     */
    public function show(Books $books)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Books  $books
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book = Books::find($id);
        $id = $id;
        return view('admin.books.edit-book', compact('book', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Books  $books
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            "inputBookAuthor"  =>  'required',
            "inputBookTitle"  =>  'required',
            "inputBookSummary"  =>  'required'
        ]);

        $book = Books::find($id);
        $book->author = $request->get('inputBookAuthor');
        $book->title = $request->get('inputBookTitle');
        $book->summary = $request->get('inputBookSummary');
        $book->expected_number_of_chapters = $request->get('inputBookExpectedChapters');
        $book->shared = $request->get('inputBookShared');
        $book->save();
        return view('admin.books.edit-book', compact('book', 'id'))->with('success', 'Book Edited');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Books  $books
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Books::find($id);
        $book->delete();
        return view('admin.admin-panel')->with('success', 'Book Deleted');
    }
}
