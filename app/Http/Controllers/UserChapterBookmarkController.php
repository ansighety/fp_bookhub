<?php

namespace App\Http\Controllers;

use App\UserChapterBookmark;
use App\Chapters;
use App\Books;
use Illuminate\Http\Request;
use DB;

class UserChapterBookmarkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($userID)
    {
        $bookmarks = DB::select('select * from user_chapter_bookmarks where user_id=:userID',['userID'=>$userID]);
        $test = 0;

        if(!empty($bookmarks))
        {
            $allBookmarks=json_decode(json_encode($bookmarks), true);

            foreach($bookmarks as $bookmark)
            {

                $bookTitle = DB::table('books')->where('id', $bookmark->book_id)->first()->value('title');

                //Get the Chapter Number
                $chapterNo = DB::table('chapters')->where('id', $bookmark->chapter_id)->first()->value('number');

                //Bookmark updated at
                $updatedAt = $bookmark->updated_at;

                $addBookmark = ([
                    'BookTitle' =>  $bookTitle,
                    'ChapterNo' =>  $chapterNo,
                    'UpdatedAt' =>  $updatedAt
                ]);

                array_push($allBookmarks, $addBookmark);
            }
        }
        else
        {
            $allBookmarks = false;
        }

        return view('user.all-bookmarks', compact('allBookmarks','test'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($userID,$chapterID)
    {
        $chapter = Chapters::find($chapterID);
        $book = Books::find($chapter->book_id);
        
        $exists = DB::select('select id from user_chapter_bookmarks where user_id=:userID AND book_id=:bookID',['userID'=>$userID,'bookID'=>$book->id]);

        if(empty($exists))
        {
            $userChapterBookmark = new UserChapterBookmark([
                'user_id'   =>  $userID,
                'book_id'   =>  $book->id,
                'chapter_id'   =>  $chapterID
            ]);
            $userChapterBookmark->save();
        }
        else
        {
            $exists=array_shift($exists);
            UserChapterBookmarkController::update($exists->id, $chapterID);
        }

        return redirect()->route('chapter-content', $chapterID);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserChapterBookmark  $userChapterBookmark
     * @return \Illuminate\Http\Response
     */
    public function show(UserChapterBookmark $userChapterBookmark)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserChapterBookmark  $userChapterBookmark
     * @return \Illuminate\Http\Response
     */
    public function edit(UserChapterBookmark $userChapterBookmark)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserChapterBookmark  $userChapterBookmark
     * @return \Illuminate\Http\Response
     */
    public function update($bookmarkID, $chapterID)
    {
        $updateBookmark = UserChapterBookmark::find($bookmarkID);
        $updateBookmark->chapter_id = $chapterID;
        $updateBookmark->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserChapterBookmark  $userChapterBookmark
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserChapterBookmark $userChapterBookmark)
    {
        //
    }
}
