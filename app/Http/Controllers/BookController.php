<?php

namespace App\Http\Controllers;

use App\Books;
use App\User;
use Illuminate\Http\Request;
use Auth;
use DB;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $book = Books::find($id);
        $user = Auth::user();
        $bookPurchases = DB::select('select * from book_purchases where user_id=:user_id and book_id=:book_id', ['user_id' => $user->id,'book_id'=>$id]);
        if($bookPurchases!=null)
        {
            $purchased = true;
        }
        else
        {
            $purchased = false;
        }
        return view('book.book-details', compact('book','user','purchased'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = Books::find($id);
        $allChapters = DB::select('select * from chapters where book_id=:book_id order by number ASC', ['book_id'=>$id]);

        if($allChapters!=null)
        {
            $allChapters = json_decode(json_encode($allChapters), true);
        }

        return view('book.book-content', compact('book','allChapters'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
