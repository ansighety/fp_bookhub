<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Books;
use App\Chapters;
use DB;

class ChapterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $chapter = Chapters::find($id);
        $book = Books::find($chapter->book_id);
        $prevChapterID = DB::select('select id from chapters where book_id=:book_id and number=:prev_chapter', ['book_id'=>$chapter->book_id,'prev_chapter'=>($chapter->number)-1]);        
        $nextChapterID = DB::select('select id from chapters where book_id=:book_id and number=:next_chapter', ['book_id'=>$chapter->book_id,'next_chapter'=>($chapter->number)+1]);

        if($prevChapterID!=null)
        {
            $prevChapterID=json_decode(json_encode($prevChapterID), true);
            $prevChapterID=array_shift($prevChapterID);
        }
        if($nextChapterID!=null)
        {
            $nextChapterID=json_decode(json_encode($nextChapterID), true);
            $nextChapterID=array_shift($nextChapterID);
        }

        $data = [
            'book'  => $book,
            'chapter'   => $chapter,
            'prevChapterID' => $prevChapterID,
            'nextChapterID' => $nextChapterID
        ];

        return view('book.chapter-content', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
