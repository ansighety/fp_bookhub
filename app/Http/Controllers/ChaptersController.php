<?php

namespace App\Http\Controllers;

use App\Chapters;
use App\Books;
use Illuminate\Http\Request;
use DB;

class ChaptersController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    
    public function getAllChapters($id)
    {
        $book = Books::find($id);
        $allChapters = DB::select('select * from chapters where book_id=:id order by number asc', ['id' => $id]);

        if($allChapters!=null)
        {
            $allChapters = json_decode(json_encode($allChapters), true);
        }

        return view('admin.books.manage-book-chapters', compact('book','allChapters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            "inputChapterContent"  =>  'required'
        ]);

        $chapter = new Chapters([
            'book_id'   =>   $request->get("inputBookID"),
            'number'   =>   $request->get("inputChapterNumber"),
            'title'   =>   $request->get("inputChapterTitle"),
            'content'   =>   $request->get("inputChapterContent"),
        ]);
        $chapter->save();
        $data = [
            'id'   =>   $request->get("inputBookID"),
            'success'  => 'Chapter Added'
        ];
        return view('admin.books.add-book-chapter')->with($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Chapters  $chapters
     * @return \Illuminate\Http\Response
     */
    public function show(Chapters $chapters)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Chapters  $chapters
     * @return \Illuminate\Http\Response
     */
    public function edit(Chapters $chapters)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Chapters  $chapters
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Chapters $chapters)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Chapters  $chapters
     * @return \Illuminate\Http\Response
     */
    public function destroy($bookID, $chapterID)
    {
        $book = Books::find($bookID);
        $chapter = Chapters::find($chapterID);
        $chapter->delete();
        $data = [
            'book'   =>   $book,
            'success'  => 'Chapter Deleted'
        ];
        return view('admin.books.manage-book-chapters')->with($data);
    }
}
