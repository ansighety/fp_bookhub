<?php

namespace App\Http\Controllers;

use App\Books;
use App\BookPurchase;
use Illuminate\Http\Request;

class BookPurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($userID, $bookID)
    {
        $bookPurchase = new BookPurchase([
            'user_id'   =>  $userID,
            'book_id'   =>  $bookID
        ]);
        $bookPurchase->save();
        $book = Books::find($bookID);
        return view('book.book-details', compact('book'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BookPurchase  $bookPurchase
     * @return \Illuminate\Http\Response
     */
    public function show(BookPurchase $bookPurchase)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BookPurchase  $bookPurchase
     * @return \Illuminate\Http\Response
     */
    public function edit(BookPurchase $bookPurchase)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BookPurchase  $bookPurchase
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BookPurchase $bookPurchase)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BookPurchase  $bookPurchase
     * @return \Illuminate\Http\Response
     */
    public function destroy(BookPurchase $bookPurchase)
    {
        //
    }
}
