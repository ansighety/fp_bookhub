@extends('layouts.app')

@push('head')
<style type='text/css'>
.chapter_header{
    margin-bottom:50px;
    text-align:center;
}
</style>
@endpush

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                
                <div class="card-header">{{$data['book']->title}} by {{$data['book']->author}}</div>

                <div class="card-body">
                    <div class="chapter_header">
                        <div class="row justify-content-center">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <h3>Chapter {{$data['chapter']->number}}</h3>
                                <h2>{{$data['chapter']->title}}</h2>
                            </div>
                            <div class="col-md-3">
                                <a href="{{ route('bookmark-chapter',[Auth::id(),$data['chapter']->id]) }}" class="btn btn-primary">Place Bookmark</a>
                            </div>
                        </div>
                    </div>
                    {!! $data['chapter']->content !!}
                    <div class="row justify-content-center" style="text-align:center;">
                        
                        <div class="col-md-4">
                        @if(!empty($data['prevChapterID']))
                            <a href="{{ route('chapter-content', ['id'=>$data['prevChapterID']['id']]) }}">Previous<br>Chapter</a>
                        @endif
                        </div>
                        <div class="col-md-4">
                            <p>Chapter {{$data['chapter']->number}}<br/>{{$data['chapter']->title}}</p>
                        </div>
                        
                        <div class="col-md-4">
                        @if(!empty($data['nextChapterID']))
                            <a href="{{ route('chapter-content', ['id'=>$data['nextChapterID']['id']]) }}">Next<br>Chapter</a>
                        @endif
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection