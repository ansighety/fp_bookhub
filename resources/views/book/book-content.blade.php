@extends('layouts.app')

@push('head')
<script type="text/javascript">
$(document).ready(function(){
    $('#dtChapters').DataTable(
        /*{"searching": false // false to disable search (or any other option)}*/
    );
    $('.dataTables_length').addClass('bs-select');
});
</script>

<style type='text/css'>
table.dataTable thead .sorting:after,
table.dataTable thead .sorting:before,
table.dataTable thead .sorting_asc:after,
table.dataTable thead .sorting_asc:before,
table.dataTable thead .sorting_asc_disabled:after,
table.dataTable thead .sorting_asc_disabled:before,
table.dataTable thead .sorting_desc:after,
table.dataTable thead .sorting_desc:before,
table.dataTable thead .sorting_desc_disabled:after,
table.dataTable thead .sorting_desc_disabled:before {
  bottom: .5em;
}
</style>
@endpush

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-body">
                    @if(!empty($book))
                        <div class="row justify-content-center">
                            <h1>{{$book['title']}}</h1>
                        </div>
                        <div class="row justify-content-center">
                            <h2>{{$book['author']}}</h2>
                        </div>

                        <h3 style="margin-top:50px; text-align:center;">Table of Contents</h3>

                        <table id="dtChapters" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th class="th-sm">Chapter No.</th>
                                    <th class="th-sm">Chapter Title</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!empty($allChapters))
                                    @foreach($allChapters as $chapter)
                                    <tr>
                                        <td>{{$chapter['number']}}</td>
                                        <td><a href="{{ route('chapter-content', ['id'=>$chapter['id']]) }}">{{$chapter['title']}}</a></td>
                                    </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="2" style="text-align:center;"><b>NO CHAPTERS YET</b></td>
                                    </tr>
                                @endif
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Chapter No.</th>
                                    <th>Chapter Title</th>
                                </tr>
                            </tfoot>
                        </table>

                        
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@endsection