@extends('layouts.app')

@push('head')

@endpush

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-body">
                    @if(!empty($book))
                        <div class="row justify-content-center">
                            <h1>{{$book['title']}}</h1>
                        </div>
                        <div class="row justify-content-center">
                            <h2>{{$book['author']}}</h2>
                        </div>

                        <div class="row justify-content-center">
                            <div class="col-md-8">
                                <nav class="nav nav-pills nav-fill">
                                    @switch($user['membership_status'])
                                        @case(0)
                                        @case(1)
                                                @if( (($book['type'] == 3) || ($book['type'] == 4)) && (!$purchased) )
                                                    <a class="nav-item nav-link" href="#">Borrow</a>
                                                @endif
                                                @if( !$purchased )
                                                    <a class="nav-item nav-link" href="{{ route('purchase-book',[Auth::id(),$book['id']]) }}">Purchase</a>
                                                @endif
                                            @break

                                        @case(2)
                                            
                                            @break

                                        @default
                                            <span>Something went wrong, please try again</span>
                                    @endswitch
                                    @if( ($book['type'] == 1) || ($book['type'] == 2) || $purchased || $user['membership_status']==2 )
                                    <a class="nav-item nav-link" href="{{ route('book-content', ['id'=>$book['id']]) }}">Read</a>
                                    @endif
                                    @if($purchased || $user['membership_status']==2 )
                                        <a class="nav-item nav-link" href="#">Download</a>
                                    @endif
                                    @if($purchased || $user['membership_status']==2 )
                                        <a class="nav-item nav-link" href="#">Add to Shelf</a>
                                    @endif
                                    <a class="nav-item nav-link" href="#">Review</a>
                                </nav>
                            </div>
                        </div>

                        
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@endsection