@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Profile Settings</div>

                <div class="card-body">
                    <!--
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                    -->

                    <div class="row">
                        <div class="col-3">
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            <a class="nav-link active" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="true">Profile</a>
                            <a class="nav-link" id="v-pills-password-tab" data-toggle="pill" href="#v-pills-password" role="tab" aria-controls="v-pills-password" aria-selected="false">Change Password</a>
                            <a class="nav-link" id="v-pills-membership-tab" data-toggle="pill" href="#v-pills-membership" role="tab" aria-controls="v-pills-membership" aria-selected="false">Membership Status</a>
                            </div>
                        </div>
                        <div class="col-9">
                            <div class="tab-content" id="v-pills-tabContent">
                                <div class="tab-pane fade show active" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">

                                    @if(!empty($errors))
                                        @if(count($errors)>0)
                                        <div class="alert alert-danger">
                                            <ul>
                                            @foreach($errors->all() as $error)
                                                <li>{{$error}}</li>
                                            @endforeach
                                            </ul>
                                        </div>
                                        @endif
                                    @endif
                                    @if(!empty($error))
                                    <div class="alert alert-danger">
                                        <ul>
                                            <li>{{$error}}</li>
                                        </ul>
                                    </div>
                                    @endif
                                    @if(!empty($success))
                                    <div class="alert alert-success">
                                        <p>{{$success}}</p>
                                    </div>
                                    @endif
                                    <form method="post" action="{{action('UserController@update', $userData['id'])}}">
                                        {{csrf_field()}}
                                        <input name="_method" type="hidden" value="PATCH">
                                        <div class="form-group row">
                                            <label for="inputUserName" class="col-sm-3 col-form-label">Name</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="inputUserName" name="inputUserName" value="{{$userData['name']}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputUserEmail" class="col-sm-3 col-form-label">Email Address</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="inputUserEmail" name="inputUserEmail" value="{{$userData['email']}}">
                                            </div>
                                        </div>
                                        <div class="dropdown-divider"></div>
                                        <div class="row">
                                            <div class="col"><small>You must key in your password before updating your profile.</small></div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputUserPassword" class="col-sm-3 col-form-label">Current Password</label>
                                            <div class="col-sm-9">
                                                <input type="password" class="form-control" id="inputUserPassword" name="inputUserPassword" placeholder="Enter password here.">
                                            </div>
                                        </div>
                                        <div class="form-group row justify-content-md-center">
                                            <div class="col-sm-auto">
                                                <button type="submit" class="btn btn-primary">Update Profile</button>
                                            </div>
                                        </div>
                                    </form>



                                </div>
                                <div class="tab-pane fade" id="v-pills-password" role="tabpanel" aria-labelledby="v-pills-password-tab">
                                    CHANGE PASSWORD HERE!
                                </div>
                                <div class="tab-pane fade" id="v-pills-membership" role="tabpanel" aria-labelledby="v-pills-membership-tab">
                                @switch($userData['membership_status'])
                                    @case(0)
                                        <h2 style="text-align:center;">You are not a Monthly Subscribed Member!</h2>
                                        <p>Subscribe now!</p>
                                        @break

                                    @case(1)
                                        <h2 style="text-align:center;">You have not payed for the latest month.</h2>
                                        <p>Make payment now.</p>
                                        @break

                                    @case(2)
                                        <h2 style="text-align:center;">You are a Monthly Subscribed Member!</h2>
                                        <p>You have access to all available books.</p>
                                        <p>Thank you for your support!</p>
                                        @break

                                    @default
                                        <span>Something went wrong, please try again</span>
                                @endswitch
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
