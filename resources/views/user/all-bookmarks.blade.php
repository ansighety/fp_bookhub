@extends('layouts.app')

@push('head')
<script type="text/javascript">
$(document).ready(function(){
    $('#dtShelves').DataTable(
        /*{"searching": false // false to disable search (or any other option)}*/
    );
    $('.dataTables_length').addClass('bs-select');
});
</script>

<style type='text/css'>
table.dataTable thead .sorting:after,
table.dataTable thead .sorting:before,
table.dataTable thead .sorting_asc:after,
table.dataTable thead .sorting_asc:before,
table.dataTable thead .sorting_asc_disabled:after,
table.dataTable thead .sorting_asc_disabled:before,
table.dataTable thead .sorting_desc:after,
table.dataTable thead .sorting_desc:before,
table.dataTable thead .sorting_desc_disabled:after,
table.dataTable thead .sorting_desc_disabled:before {
  bottom: .5em;
}
</style>
@endpush

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">All Shelves</div>

                <div class="card-body">

                    <div class="card-body">

                        @if(count($errors)>0)
                        <div class="alert alert-danger">
                            <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                            </ul>
                        </div>
                        @endif
                        @if(!empty($success))
                        <div class="alert alert-success">
                            <p>{{$success}}</p>
                        </div>
                        @endif

                        <?php echo "<script>console.log(".json_encode($test).")</script>" ?>

                        <h3 style="margin:30px 0px; text-align:center;">All Bookmarks</h3>

                        <table id="dtShelves" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th class="th-sm">Book Title</th>
                                    <th class="th-sm">Chapter Number</th>
                                    <th class="th-sm">Bookmarked On</th>
                                    <th class="th-sm">Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!empty($allBookmarks))
                                    @foreach($allBookmarks as $bookmark)
                                    <tr>
                                        <td>Book Title</td>
                                        <td>Chapter Number</td>
                                        <td>Bookmarked On</td>
                                        <td>Delete</td>
                                    </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4" style="text-align:center;"><b>NONE</b> - add a new bookmark now!</td>
                                    </tr>
                                @endif
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Book Title</th>
                                    <th>Chapter Number</th>
                                    <th>Bookmarked On</th>
                                    <th>Delete</th>
                                </tr>
                            </tfoot>
                        </table>



                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection