@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Existing Shelf</div>

                <div class="card-body">

                    @if(count($errors)>0)
                    <div class="alert alert-danger">
                        <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                        </ul>
                    </div>
                    @endif
                    @if(!empty($success))
                    <div class="alert alert-success">
                        <p>{{$success}}</p>
                    </div>
                    @endif
                    <form method="post" action="{{action('ShelfController@update', $id)}}">
                        {{csrf_field()}}
                        <input name="_method" type="hidden" value="PATCH">
                        <div class="form-group row">
                            <label for="inputShelfName" class="col-sm-3 col-form-label">Shelf Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="inputShelfName" name="inputShelfName" value="{{$shelf['name']}}">
                            </div>
                        </div>
                        <div class="form-group row justify-content-md-center">
                            <div class="col-sm-auto">
                                <button type="submit" class="btn btn-primary">Edit Shelf</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
