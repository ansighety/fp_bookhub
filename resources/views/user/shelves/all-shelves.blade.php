@extends('layouts.app')

@push('head')
<script type="text/javascript">
$(document).ready(function(){
    $('#dtShelves').DataTable(
        /*{"searching": false // false to disable search (or any other option)}*/
    );
    $('.dataTables_length').addClass('bs-select');
});
</script>

<style type='text/css'>
table.dataTable thead .sorting:after,
table.dataTable thead .sorting:before,
table.dataTable thead .sorting_asc:after,
table.dataTable thead .sorting_asc:before,
table.dataTable thead .sorting_asc_disabled:after,
table.dataTable thead .sorting_asc_disabled:before,
table.dataTable thead .sorting_desc:after,
table.dataTable thead .sorting_desc:before,
table.dataTable thead .sorting_desc_disabled:after,
table.dataTable thead .sorting_desc_disabled:before {
  bottom: .5em;
}
</style>
@endpush

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">All Shelves</div>

                <div class="card-body">

                    <div class="card-body">

                        @if(count($errors)>0)
                        <div class="alert alert-danger">
                            <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                            </ul>
                        </div>
                        @endif
                        @if(!empty($success))
                        <div class="alert alert-success">
                            <p>{{$success}}</p>
                        </div>
                        @endif

                        <div class="row justify-content-center">
                            <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseNewShelf" aria-expanded="false" aria-controls="collapseNewShelf">
                                New Shelf
                            </button>
                        </div>

                        <div class="row justify-content-center">
                            <div class="collapse" id="collapseNewShelf"><div class="card card-body">
                                <form method="post" action="{{action('ShelfController@store')}}">
                                    {{csrf_field()}}
                                    <input id="inputUserID" type="text" name="inputUserID" value="{{$userID}}">
                                    <div class="form-group row">
                                        <label for="inputShelfName" class="col-sm-3 col-form-label">Shelf Name</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="inputShelfName" name="inputShelfName" placeholder="Shelf Name Here">
                                        </div>
                                    </div>
                                    <div class="form-group row justify-content-md-center">
                                        <div class="col-sm-auto">
                                            <button type="submit" class="btn btn-primary">Add Shelf</button>
                                        </div>
                                    </div>
                                </form>
                                
                            </div></div>
                        </div>

                        <h3 style="margin:30px 0px; text-align:center;">All Shelves</h3>

                        <table id="dtShelves" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                <th class="th-sm">Shelf ID</th>
                                <th class="th-sm">Name</th>
                                <th class="th-sm">Edit</th>
                                <th class="th-sm">Manage</th>
                                <th class="th-sm">Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!empty($allShelves))
                                    @foreach($allShelves as $shelf)
                                    <tr>
                                        <td>{{$shelf['id']}}</td>
                                        <td>{{$shelf['name']}}</td>
                                        <td><a href="{{action('ShelfController@edit', $shelf['id'])}}">Edit Details</a></td>
                                        <td>Books</td>
                                        
                                        <td>
                                            <form method="post" class="delete_form" action="{{ route('delete-shelf',[Auth::id(),$shelf['id']]) }}">
                                                {{csrf_field()}}
                                                <input name="_method" type="hidden" value="DELETE">
                                                <button type="submit" class="btn btn-danger">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5" style="text-align:center;"><b>NONE</b> - add a new shelf now!</td>
                                    </tr>
                                @endif
                            </tbody>
                            <tfoot>
                                <tr>
                                <th>Shelf ID</th>
                                <th>Name</th>
                                <th>Edit</th>
                                <th>Manage</th>
                                <th>Delete</th>
                                </tr>
                            </tfoot>
                        </table>



                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection