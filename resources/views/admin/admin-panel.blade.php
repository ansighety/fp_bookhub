@extends('layouts.app')

@push('head')
<script type="text/javascript">
$(document).ready(function(){
    $('.delete_form').on('submit',function(){
        if(confirm("Are you sure you want to delete it?"))
        {
            return true;
        }
        else
        {
            return false;
        }
    });
});
</script>
@endpush

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Admin Panel</div>

                <div class="card-body">
                    <!--
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                    -->

                    <div class="row">
                        <div class="col-3">
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            <a class="nav-link active" id="v-pills-books-tab" data-toggle="pill" href="#v-pills-books" role="tab" aria-controls="v-pills-books" aria-selected="true">Books</a>
                            <a class="nav-link" id="v-pills-series-tab" data-toggle="pill" href="#v-pills-series" role="tab" aria-controls="v-pills-series" aria-selected="false">Series</a>
                            </div>
                        </div>
                        <div class="col-9">
                            <div class="tab-content" id="v-pills-tabContent">
                                <div class="tab-pane fade show active" id="v-pills-books" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                    
                                    <p style="text-align:center;">
                                        <a class="btn btn-primary" type="button" href="{{url('add-book')}}" role="button">Add New Book</a>
                                    </p>

                                    <h3 style="margin-top:50px; text-align:center;">All Books</h3>

                                    @if($message = Session::get('success'))
                                    <div class="alert alert-success">
                                        <p>{{$message}}</p>
                                    </div>
                                    @endif

                                    <table class="table">
                                        <thead class="thead-light">
                                            <tr>
                                            <th scope="col">Book Title</th>
                                            <th scope="col">Author</th>
                                            <th scope="col">Edit Details</th>
                                            <th scope="col">Chapters</th>
                                            <th scope="col">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(!empty($allBooks))
                                                @foreach($allBooks as $book)
                                                <tr>
                                                    <td>{{$book['title']}}</td>
                                                    <td>{{$book['author']}}</td>
                                                    <td><a href="{{action('BooksController@edit', $book['id'])}}">Edit Details</a></td>
                                                    
                                                    <td><a href="{{ route('display-chapters', ['id'=>$book['id']]) }}">Manage</a></td>
                                                    <td>
                                                        <form method="post" class="delete_form" action="{{action('BooksController@destroy', $book['id'])}}">
                                                        {{csrf_field()}}
                                                        <input name="_method" type="hidden" value="DELETE">
                                                        <button type="submit" class="btn btn-danger">Delete</button>
                                                        </form>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="5" style="text-align:center;"><b>NONE</b> - add a new book now!</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>

                                </div>
                                <div class="tab-pane fade" id="v-pills-series" role="tabpanel" aria-labelledby="v-pills-series-tab">
                                    
                                    <p style="text-align:center;">
                                        <a class="btn btn-primary" type="button" href="{{url('/add-series')}}" role="button">Add New Series</a>
                                    </p>

                                    <h3 style="margin-top:50px; text-align:center;">All Series</h3>

                                    @if($message = Session::get('success'))
                                    <div class="alert alert-success">
                                        <p>{{$message}}</p>
                                    </div>
                                    @endif

                                    <table class="table">
                                        <thead class="thead-light">
                                            <tr>
                                            <th scope="col">Series Title</th>
                                            <th scope="col">Author</th>
                                            <th scope="col">Edit Details</th>
                                            <th scope="col">Books</th>
                                            <th scope="col">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(!empty($allSeries))
                                                @foreach($allSeries as $series)
                                                <tr>
                                                    <td>{{$series['title']}}</td>
                                                    <td>{{$series['author']}}</td>
                                                    <td><a href="{{action('SeriesController@edit', $series['id'])}}">Edit Details</a></td>
                                                    
                                                    <td><a href="{{ route('display-series-books', ['id'=>$series['id']]) }}">Manage</a></td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="5" style="text-align:center;"><b>NONE</b> - add a new book now!</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection