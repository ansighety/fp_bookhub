@extends('layouts.app')

@push('head')
<script type="text/javascript">
$(document).ready(function(){
    $('.delete_form').on('submit',function(){
        if(confirm("Are you sure you want to delete it?"))
        {
            return true;
        }
        else
        {
            return false;
        }
    });
});
</script>
@endpush

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Manage Book Chapters</div>

                <div class="card-body">
                    <div>
                        <h2 style="text-align:center;">{{$book['title']}}</h2>
                        <h3 style="text-align:center;">{{$book['author']}}</h3>
                    </div>
                    <p style="margin:30px 0px; text-align:center;">                    
                        <a class="btn btn-primary" type="button" href="{{ route('new-chapter', ['id'=>$book['id']]) }}" role="button">Add New Chapter</a>
                    </p>

                    <h3 style="text-align:center;">All Chapters</h3>

                    @if(count($errors)>0)
                    <div class="alert alert-danger">
                        <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                        </ul>
                    </div>
                    @endif

                    @if(!empty($success))
                    <div class="alert alert-success">
                        <p>{{$success}}</p>
                    </div>
                    @endif

                    <table class="table">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">Chapter No.</th>
                                <th scope="col">Chapter Title</th>
                                <th scope="col">Edit</th>
                                <th scope="col">Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($allChapters))
                                @foreach($allChapters as $chapter)
                                    <tr>
                                        <td>{{$chapter['number']}}</td>
                                        <td>{{$chapter['title']}}</td>
                                        <td><a href="{{action('ChaptersController@edit', $chapter['id'])}}">Edit Chapter</a></td>
                                        <!--
                                        <td>
                                            <form method="post" class="delete_form" action="{{ route('delete-chapter',['bookID'=>$book['id'],'chapterID'=>$chapter['id']]) }}">
                                                {{csrf_field()}}
                                                {{ method_field('DELETE') }}
                                                <input type="hidden" name="_method" value="DELETE">
                                                <button type="submit" class="btn btn-danger">Delete</button>
                                            </form>
                                        </td>
                                        -->
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4" style="text-align:center;"><b>NONE</b> - add a new chapter now!</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection