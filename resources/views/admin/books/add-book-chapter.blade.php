@extends('layouts.app')

@push('head')
<script>
    /*
      This function gets the content from the instance of Textbox.io
      with the ID 'textbox'
    */
    var getEditorContent = function(){
        var editors = textboxio.get('#inputChapterContent');
        var editor = editors[0];
        return editor.content.get();
    };
</script>
@endpush

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Add New Chapter</div>

                <div class="card-body">

                    @if(count($errors)>0)
                    <div class="alert alert-danger">
                        <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                        </ul>
                    </div>
                    @endif
                    @if(!empty($success))
                    <div class="alert alert-success">
                        <p>{{$success}}</p>
                    </div>
                    @endif
                    <form method="post" action="{{action('ChaptersController@store')}}">
                        {{csrf_field()}}
                        <input id="inputBookID" type="text" name="inputBookID" value="{{$id}}">
                        <div class="form-group row">
                            <label for="inputChapterNumber" class="col-sm-3 col-form-label">Chapter Number</label>
                            <div class="col-sm-9">
                                <input type="number" id="inputChapterNumber" name="inputChapterNumber" min="1">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputChapterTitle" class="col-sm-3 col-form-label">Chapter Title</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="inputChapterTitle" name="inputChapterTitle" placeholder="Chapter Title Here">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputChapterContent" class="col-sm-3 col-form-label">Chapter Content</label>
                        </div>
                        <div class="form-group row">
                            <div class="col">
                                <textarea style="width: 100%; height: 800px;" class="form-control" id="inputChapterContent" name="inputChapterContent" placeholder="Write the chapter content here."></textarea>
                            </div>
                        </div>
                        <div class="form-group row justify-content-md-center">
                            <div class="col-sm-auto">
                                <button type="submit" class="btn btn-primary">Add Chapter</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection