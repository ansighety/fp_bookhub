@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Existing Book</div>

                <div class="card-body">

                    @if(count($errors)>0)
                    <div class="alert alert-danger">
                        <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                        </ul>
                    </div>
                    @endif
                    @if(!empty($success))
                    <div class="alert alert-success">
                        <p>{{$success}}</p>
                    </div>
                    @endif
                    <form method="post" action="{{action('BooksController@update', $id)}}">
                        {{csrf_field()}}
                        <input name="_method" type="hidden" value="PATCH">
                        <div class="form-group row">
                            <label for="inputBookAuthor" class="col-sm-3 col-form-label">Book Author</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="inputBookAuthor" name="inputBookAuthor" value="{{$book['author']}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputBookTitle" class="col-sm-3 col-form-label">Book Title</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="inputBookTitle" name="inputBookTitle" value="{{$book['title']}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputBookSummary" class="col-sm-3 col-form-label">Book Summary</label>
                            <div class="col-sm-9">
                                <textarea style="width: 100%; height: 300px;" class="form-control" id="inputBookSummary" name="inputBookSummary" placeholder="Write a summary of the book here.">{{$book['summary']}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputBookExpectedChapters" class="col-sm-3 col-form-label">Expected Number of Chapters</label>
                            <div class="col-sm-9">
                                <input type="number" id="inputBookExpectedChapters" name="inputBookExpectedChapters" min="1" value="{{$book['expected_number_of_chapters']}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-3">Share Book?</div>
                            <div class="col-sm9">
                                <div class="form-check">
                                    <input id="inputBookShared" type="hidden" value="0" name="inputBookShared" value="0">
                                    <input class="form-check-input" type="checkbox" id="inputBookShared" name="inputBookShared" value="1"
                                    @if($book['shared']==1)
                                    checked
                                    @endif
                                    >

                                </div>
                            </div>
                        </div>
                        <div class="form-group row justify-content-md-center">
                            <div class="col-sm-auto">
                                <button type="submit" class="btn btn-primary">Edit Book</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
