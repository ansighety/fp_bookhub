@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Existing Series</div>

                <div class="card-body">

                    @if(count($errors)>0)
                    <div class="alert alert-danger">
                        <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                        </ul>
                    </div>
                    @endif
                    @if(!empty($success))
                    <div class="alert alert-success">
                        <p>{{$success}}</p>
                    </div>
                    @endif
                    <form method="post" action="{{action('SeriesController@update', $id)}}">
                        {{csrf_field()}}
                        <input name="_method" type="hidden" value="PATCH">
                        <div class="form-group row">
                            <label for="inputSeriesAuthor" class="col-sm-3 col-form-label">Series Author</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="inputSeriesAuthor" name="inputSeriesAuthor" value="{{$series['author']}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputSeriesTitle" class="col-sm-3 col-form-label">Series Title</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="inputSeriesTitle" name="inputSeriesTitle" value="{{$series['title']}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputSeriesSummary" class="col-sm-3 col-form-label">Series Summary</label>
                            <div class="col-sm-9">
                                <textarea style="width: 100%; height: 300px;" class="form-control" id="inputSeriesSummary" name="inputSeriesSummary" placeholder="Write a summary of the series here.">{{$series['summary']}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row justify-content-md-center">
                            <div class="col-sm-auto">
                                <button type="submit" class="btn btn-primary">Edit Book</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
