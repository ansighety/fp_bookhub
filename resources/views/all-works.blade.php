@extends('layouts.app')

@push('head')
<script type="text/javascript">
$(document).ready(function(){
    $('#dtSeries').DataTable(
        /*{"searching": false // false to disable search (or any other option)}*/
    );
    $('#dtBooks').DataTable(
        /*{"searching": false // false to disable search (or any other option)}*/
    );
    $('.dataTables_length').addClass('bs-select');
});
</script>

<style type='text/css'>
table.dataTable thead .sorting:after,
table.dataTable thead .sorting:before,
table.dataTable thead .sorting_asc:after,
table.dataTable thead .sorting_asc:before,
table.dataTable thead .sorting_asc_disabled:after,
table.dataTable thead .sorting_asc_disabled:before,
table.dataTable thead .sorting_desc:after,
table.dataTable thead .sorting_desc:before,
table.dataTable thead .sorting_desc_disabled:after,
table.dataTable thead .sorting_desc_disabled:before {
  bottom: .5em;
}
</style>
@endpush

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">All E-Books</div>

                <div class="card-body">

                    <div class="row">
                        <div class="col-3">
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <a class="nav-link active" id="v-pills-series-tab" data-toggle="pill" href="#v-pills-series" role="tab" aria-controls="v-pills-series" aria-selected="false">Based on Series</a>
                                <a class="nav-link" id="v-pills-books-tab" data-toggle="pill" href="#v-pills-books" role="tab" aria-controls="v-pills-books" aria-selected="true">All Books</a>
                            </div>
                        </div>
                        <div class="col-9">
                            <div class="tab-content" id="v-pills-tabContent">
                                <div class="tab-pane fade show active" id="v-pills-series" role="tabpanel" aria-labelledby="v-pills-series-tab">

                                    <h3 style="margin-top:50px; text-align:center;">All Series</h3>

                                    <table id="dtSeries" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                            <th class="th-sm">Series Title</th>
                                            <th class="th-sm">Author</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(!empty($allSeries))
                                                @foreach($allSeries as $series)
                                                <tr>
                                                    <td>{{$series['title']}}</td>
                                                    <td>{{$series['author']}}</td>
                                                </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="2" style="text-align:center;"><b>NONE</b> - add a new book now!</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                            <th>Book Title</th>
                                            <th>Author</th>
                                            </tr>
                                        </tfoot>
                                    </table>

                                </div>
                                <div class="tab-pane fade" id="v-pills-books" role="tabpanel" aria-labelledby="v-pills-profile-tab">

                                    <h3 style="margin-top:50px; text-align:center;">All Books</h3>

                                    <table id="dtBooks" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                            <th class="th-sm">Book Title</th>
                                            <th class="th-sm">Author</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(!empty($allBooks))
                                                @foreach($allBooks as $book)
                                                <tr>
                                                    <td><a href="{{ route('book-details', ['id'=>$book['id']]) }}">{{$book['title']}}</a></td>
                                                    <td>{{$book['author']}}</td>
                                                </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="2" style="text-align:center;"><b>NONE</b> - add a new book now!</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                            <th>Book Title</th>
                                            <th>Author</th>
                                            </tr>
                                        </tfoot>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection