<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

/** GENERAL USER ROUTES */
Route::get('/', function () {
    return view('welcome');
});
/*Route::view('/', 'welcome');*/

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admin-panel', 'AdminController@admin')->middleware('is_admin')->name('admin');

Route::resource('user', 'UserController');

Route::get('/profile-settings', function () {
    $userID = Auth::id();
    return redirect()->action('UserController@show', ['id' => $userID]);
});

/** GENERAL VIEW ROUTES */
Route::get('/all-works', 'BooksController@getAllWorks')->name('all-works');

/** GENERAL BOOK ROUTES */
Route::resource('book', 'BookController');
Route::get('/book-details/{id}', 'BookController@index')->name('book-details');

Route::resource('bookpurchase', 'BookPurchaseController');
Route::get('/purchase-book/{userID}/{bookID}', 'BookPurchaseController@store')->name('purchase-book');

/** GENERAL CHAPTER ROUTES */
Route::resource('chapter', 'ChapterController');
Route::get('/book-content/{bookID}','BookController@show')->name('book-content');
Route::get('/chapter-content/{chapterID}','ChapterController@show')->name('chapter-content');

Route::resource('chapter-bookmark', 'UserChapterBookmarkController');

Route::get('/bookmark-chapter/{userID}/{bookID}', 'UserChapterBookmarkController@store')->name('bookmark-chapter');

Route::get('/all-bookmarks/{userID}', 'UserChapterBookmarkController@index')->name('all-bookmarks');

/** GENERAL SHELVES ROUTES */
Route::resource('shelf', 'ShelfController');

Route::get('/all-shelves', function(){
    $userID = Auth::id();
    return redirect()->action('ShelfController@show', ['id' => $userID]);
})->name('all-shelves');

Route::get('/delete-shelf/{userID}/{shelfID}', 'ShelfController@destroy')->name('delete-shelf');

/** ADMIN ROUTES */

Route::get('/add-book', function () {
    return view('admin.books.add-book');
})->middleware('is_admin');

Route::resource('books', 'BooksController')->middleware('is_admin');
Route::resource('chapter', 'ChaptersController')->middleware('is_admin');
Route::resource('series', 'SeriesController')->middleware('is_admin');

Route::get('/display-chapters/{id}', 'ChaptersController@getAllChapters')->middleware('is_admin')->name('display-chapters');
Route::get('/new-chapter/{id}', function ($id) {
    return view('admin.books.add-book-chapter')->with('id',$id);
})->middleware('is_admin')->name('new-chapter');

Route::get('/delete-chapters/{bookID}/{chapterID}', 'ChaptersController@destroy')->middleware('is_admin')->name('delete-chapter');

Route::get('/add-series', function () {
    return view('admin.series.add-series');
})->middleware('is_admin');

Route::get('/display-series-books/{id}', 'SeriesController@getAllBooks')->middleware('is_admin')->name('display-series-books');